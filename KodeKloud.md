## First challange
- Make sure the timezone is set correctly on App Server 1 in Africa/Brazzville
### The command to use
```
#SSH into the App server1
ssh user@ip
sudo timedatectl set-timezone Africa/Brazzaville
```
[Source to learn more about the timedatectl command](http://www.softpanorama.org/Admin/Bulletin/admin2019.shtml)